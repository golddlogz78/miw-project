import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Time extends StatefulWidget {
  const Time({super.key});

  @override
  State<Time> createState() => _TimeState();
}

class _TimeState extends State<Time> {
  // create datetime variable
  DateTime now = DateTime.now();
  TimeOfDay _timeOfDay = TimeOfDay.now();
  final firestore = FirebaseFirestore.instance.collection('Reserve_Time');

  Map timeOfDayToFirebase(TimeOfDay timeOfDay) {
    return {'hour': timeOfDay.hour, 'minute': timeOfDay.minute};
  }

  TimeOfDay firebaseToTimeOfDay(Map data) {
    return TimeOfDay(hour: data['hour'], minute: data['minute']);
  }

  // show date picker method
  void _showDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2025),
    ).then((value) {
      setState(() {
        now = value!;
      });
    });
  }

  // show time picker method
  void _showTimePicker() {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      setState(() {
        _timeOfDay = value!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // display chosen date
          Text(
            DateFormat("yyyy/MM/dd").format(now),
            style: const TextStyle(fontSize: 20),
          ),

          // display the chosen time
          Text(
            _timeOfDay.format(context).toString(),
            style: const TextStyle(fontSize: 20),
          ),

          // button
          MaterialButton(
            onPressed: _showDatePicker,
            color: Colors.blue,
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Pick a date',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ),
          ),

          MaterialButton(
            onPressed: _showTimePicker,
            color: Colors.blue,
            child: const Padding(
              padding: EdgeInsets.all(20.0),
              child: Text(
                'Pick time',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          )
        ],
      )),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.all(16),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                minimumSize: const Size(100, 45),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0))),
            onPressed: () async {
              sendDataToFirebase();

              Navigator.pushNamed(context, '/third');
            },
            child: const Text("Confirm")),
      ),
    );
  }

  void sendDataToFirebase() {
    FirebaseFirestore.instance.collection('Reserve_Time').add({
      'timestamp': now,
      'time': timeOfDayToFirebase(_timeOfDay),
    });
  }
}
