import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/firebase_options.dart';
import 'package:flutter_application_1/timepage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: true,
      theme: ThemeData(brightness: Brightness.dark),
      title: 'Named Routes Demo',
      // Start the app with the "/" named route. In this case, the app starts
      // on the FirstScreen widget.
      initialRoute: '/',
      routes: {
        '/': (context) => const Welcome(),
        '/second': (context) => const Home(),
        '/third': (context) => const List(),
        '/text': (context) => const Bar(),
        '/them': (context) => const Yerm(),
        '/sara': (context) => const Sarabun(),
        '/you': (context) => const Young(),
        '/tawan': (context) => const Tawan(),
        '/brary': (context) => const Libary(),
        '/time': (context) => const Time(),
      },
    ),
  );
}

class Welcome extends StatefulWidget {
  const Welcome({super.key});

  @override
  State<Welcome> createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 221, 190, 106),
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 208, 161, 30),
          //title: const Text('Lกฮ lism'),
          centerTitle: true,
        ),
        body: Center(
            // padding: EdgeInsets.all(15),

            child: Column(children: [
          Container(
            height: 300,
            width: 300,
            decoration: const BoxDecoration(),
          ),
          Padding(
              padding: const EdgeInsets.all(15),
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(children: [
                      TextButton(
                        onPressed: () => {},
                        style: TextButton.styleFrom(
                            minimumSize: const Size(327, 50),
                            // padding: EdgeInsets.all(15),

                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            backgroundColor:
                                const Color.fromARGB(255, 208, 161, 30)),
                        child: TextButton(
                          child: const Text(
                            'Lกฮ lism',
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/second');
                          },
                        ),
                      )
                    ]),
                  )
                ],
              )))
        ])));
  }
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 248, 218, 136),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 208, 161, 30),
          title: const Text(
            'Lกฮ lism',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(children: [
                  TextButton(
                    onPressed: () => {},
                    style: TextButton.styleFrom(
                        minimumSize: const Size(327, 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        backgroundColor:
                            const Color.fromARGB(255, 208, 161, 30)),
                    child: TextButton(
                      child: const Text(
                        'ร้านเหล้า',
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/third');
                      },
                    ),
                  )
                ]),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class List extends StatefulWidget {
  const List({super.key});

  @override
  State<List> createState() => _ListState();
}

class _ListState extends State<List> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 248, 218, 136),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 208, 161, 30),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/second');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'ร้านเหล้า',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
              child: Column(children: [
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      // padding: EdgeInsets.all(15),

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'มานีมีวัง(วังหลัง)',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/text');
                    },
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      // padding: EdgeInsets.all(15),

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'Yerm เยิ้ม ',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/them');
                    },
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      // padding: EdgeInsets.all(15),

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'Sarabun(สารบัญ)',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/sara');
                    },
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      // padding: EdgeInsets.all(15),

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'Youngโสด เชียงราย',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/you');
                    },
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'ตะวันแดงมหาชน เชียงราย',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/tawan');
                    },
                  ),
                )
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                TextButton(
                  onPressed: () => {},
                  style: TextButton.styleFrom(
                      minimumSize: const Size(327, 50),
                      // padding: EdgeInsets.all(15),

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      backgroundColor: const Color.fromARGB(255, 208, 161, 30)),
                  child: TextButton(
                    child: const Text(
                      'The Library ChiangRai',
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/brary');
                    },
                  ),
                )
              ]),
            ),
          ])),
        ),
      ),
    );
  }
}

class Bar extends StatefulWidget {
  const Bar({super.key});

  @override
  State<Bar> createState() => _BarState();
}

class _BarState extends State<Bar> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'มานีมีวัง(วังหลัง)',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(8.0),
              child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      child: Image.network(
                          'https://fastly.4sqi.net/img/general/600x600/74177238_d6DndQIajEMmBOSWEKs3l4vrM4H8eDDI61S2bcezDUs.jpg',
                          width: 300,
                          height: 250,
                          fit: BoxFit.fill),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'ต.ท่าสุด อ.เมือง จ.เชียงราย ร้านตั้งอยู่หน้ามหาวิทยาลัยแม่ฟ้าหลวง เปิดทุกวัน 19:00 - 00:00',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {
                Navigator.pushNamed(context, '/time');
              },
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}

class Yerm extends StatefulWidget {
  const Yerm({super.key});

  @override
  State<Yerm> createState() => _YermState();
}

class _YermState extends State<Yerm> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'Yerm เยิ้ม',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(8.0),
              child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      child: Image.network(
                          'https://scontent.fkkc1-1.fna.fbcdn.net/v/t39.30808-6/299309324_594719272448963_3138330803444638306_n.png?_nc_cat=101&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFvEGMDQ5h75v-gg90T1GVz0wd4GpVUUDDTB3galVRQMMKSroSmns9FocJygHEhxWHPMnvShp3ee6t5NgYimV4u&_nc_ohc=n9GvjyrzLcMAX8A9Zfy&_nc_ht=scontent.fkkc1-1.fna&oh=00_AfDLToTMg5Vb1S7HD_kKi9Nm0y2z-r9fXQW1liLZxH2ksg&oe=647D784D',
                          width: 150,
                          height: 150,
                          fit: BoxFit.fill),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'เยิ้ม ร้านเหล้าหน้ามหาวิทยาลัยแม่ฟ้าหลวง เปิดทุกวันเวลา 19:00 - 00:00',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {},
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}

class Sarabun extends StatefulWidget {
  const Sarabun({super.key});

  @override
  State<Sarabun> createState() => _SarabunState();
}

class _SarabunState extends State<Sarabun> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'Sarabun(สารบัญ)',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(8.0),
              child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      child: Image.network(
                          'https://scontent.fkkc1-1.fna.fbcdn.net/v/t39.30808-6/340274492_1892173314487818_1206028643243896362_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeHs4tKACDdO-vlc6U4YEt-R_lT6Qhi9CG3-VPpCGL0IbXzRJ-oW5ppm-KNxoVZNRZ3-E7-coQLu84INr-4Lr514&_nc_ohc=YTay1ReVeTIAX-APsO7&_nc_ht=scontent.fkkc1-1.fna&oh=00_AfApaqory9qq3UWlkNUOUvMjlQQUXrWoKaHn7uXjDcU8WA&oe=647CF55C',
                          width: 250,
                          height: 150,
                          fit: BoxFit.fill),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'Sarabun (สารบัญ) สกายบาร์แห่งใหม่ใจกลางเมืองเชียงราย ตั้งอยู่บนชั้นดาดฟ้าของร้าน The Library Chiangrai  บรรยากาศดีชิลมากๆ เหมาะกับการมาแฮงค์เอ้าท์กับเพื่อนๆเป็นที่สุด ที่นี่มีจำหน่ายอาหารและเครื่องดื่มหลายชนิด ⏰ทางร้านเปิดให้บริการตั้งแต่เวลา: 18.00 เป็นต้นไป 📍พิกัดที่ร้านสารบัญ ชั้น 4 ของร้าน The Library Chiang Rai',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {},
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}

class Young extends StatefulWidget {
  const Young({super.key});

  @override
  State<Young> createState() => _YoungState();
}

class _YoungState extends State<Young> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'Youngโสด เชียงราย',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            margin: const EdgeInsets.all(8.0),
            child: Card(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              child: Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0),
                    ),
                    child: Image.network(
                        'https://scontent.fkkc1-1.fna.fbcdn.net/v/t39.30808-6/327372006_3030042903966694_8197359877376877759_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeEvwz8SKOwobg8Y3i5MsElcouHMui_d2pmi4cy6L93ameLS-4-U_hZYQ25Eopxzuo4sEDE4TUZlI6FdF7sIoZup&_nc_ohc=3WrpoA0gTzoAX9XRZpd&_nc_ht=scontent.fkkc1-1.fna&oh=00_AfBZDelDSSvlu9QdLnK7ZegFlIJDSQU7W7x-1I6sIdzmcQ&oe=647D8839',
                        width: 250,
                        height: 150,
                        fit: BoxFit.fill),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      '60/18 หมู่ 13 ถนน ราษฎรบูรณะ ซอย 1  เทศบาลนครเชียงราย, จังหวัดเชียงราย 57000 ร้านเปิดทุกวัน เวลา 19:00 - 00:00',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {},
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}

class Tawan extends StatefulWidget {
  const Tawan({super.key});

  @override
  State<Tawan> createState() => _TawanState();
}

class _TawanState extends State<Tawan> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'ตะวันแดงมหาซน เชียงราย',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(8.0),
              child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      child: Image.network(
                          'https://scontent.fkkc1-1.fna.fbcdn.net/v/t39.30808-6/329781858_522479436537432_3742945310505215238_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeGk4FOKNZw0BYfISuWpnx-wd7EKuJr4WHp3sQq4mvhYeqHEsj2EibZPcreJIgjr3avwEQU0RDuvGzdgoA8LRR30&_nc_ohc=hN0wXES5BxcAX_0H3LB&_nc_ht=scontent.fkkc1-1.fna&oh=00_AfA3fr7t8IAH1277xBFvBUE-KU6FgyEQPurIaC4NLRGVcw&oe=647BB38E',
                          width: 250,
                          height: 250,
                          fit: BoxFit.fill),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        '174/9 หมู่ 13 ตำบลรอบเวียง อำเภอเมืองเชียงราย , Chiang Rai, Thailand, Chiang Rai เปิดทุกวันเวลา 19:00 -01:00     ☎️สอบถามรายละเอียดเพิ่มเติมและสำรองจองโต๊ะ  โทร:081-8851131Line :@twd-cr **ติดตามกิจกรรมดีๆ ได้ที่** Website:tawandang-mahason.com Facebook:ตะวันแดง มหาซน เชียงราย tiktok : @tawandang_cr',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {},
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}

class Libary extends StatefulWidget {
  const Libary({super.key});

  @override
  State<Libary> createState() => _LibaryState();
}

class _LibaryState extends State<Libary> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 219, 213, 198),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 223, 179, 57),
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/third');
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: const Text(
            'The Library ChiangRai',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(8.0),
              child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                      child: Image.network(
                          'https://scontent.fkkc1-1.fna.fbcdn.net/v/t39.30808-6/344025887_1490201831383924_9215161493691350536_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHz87OP3TMcWyCcwQ1qizLc5s6AC8lOz4bmzoALyU7PhmUnnq3qT5bsWj0t1buEgoDJT3ob_p7p61Nc59KEkW6p&_nc_ohc=5GDCyTJK9xQAX_d943t&_nc_ht=scontent.fkkc1-1.fna&oh=00_AfAOWNUnLyajnrIouiiP7LejMbJAPuKRg78rLv28oDpG6w&oe=647D4A97',
                          width: 250,
                          height: 250,
                          fit: BoxFit.fill),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'Ratbumroong Rd , Amphoe Mueang Chiang Rai,LINE:https://lin.ee/6wN9hdD ( @LibraryChiangRai )TEL: 080-951-5679 ร้านเปิดทุกวัน 20:00 - 02:00',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(color: Colors.white, boxShadow: []),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(100, 45),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0))),
              onPressed: () {},
              child: const Text("Book Now")),
        ),
      ),
    );
  }
}
